package tea.domaine;

import java.util.Objects;

public class Role {
	
	private String identifier;
	private String roleName;
	
	public Role(String identifier, String roleName) {

		this.identifier = identifier;
		this.roleName = roleName;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getRoleName() {
		return roleName;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(identifier, roleName);
	}
	
	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		
		if (object instanceof Role) {
			Role that = (Role) object;
			
			return Objects.equals(this.identifier, that.identifier)
					&& Objects.equals(this.roleName, that.roleName);
		}
		
		return false;
	}
}
