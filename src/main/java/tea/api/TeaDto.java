package tea.api;

public class TeaDto {
	
	public final String teaId;
	public final String teaName;
	public final String teaCategory;
	public final String teaDescription;
	
	public TeaDto(String teaId, String teaName, String teaCategory, String teaDescription) {
		this.teaId = teaId;
		this.teaName = teaName;
		this.teaCategory = teaCategory;
		this.teaDescription = teaDescription;
	}	
}
