package tea.infra;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class RoleEntity {

	@Id
	public String id;
	
	@Column
	public String roleName;
	
	@OneToMany(targetEntity= UserEntity.class, mappedBy="role", cascade = CascadeType.ALL)
	Set<UserEntity> users;

	public RoleEntity() {
		
	}
	
	public RoleEntity(String identifier, String roleName) {
		this.id = identifier;
		this.roleName = roleName;
	}
}
