package tea.infra;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import tea.controller.errors.ObjectNotFoundException;
import tea.domaine.User;
import tea.domaine.UserRepository;

@Repository
public class UserRepositoryHibernate implements UserRepository {
	
	private final UserDao dao;
	private final UserEntityConverter converter;

	public UserRepositoryHibernate(UserDao dao, UserEntityConverter converter) {
		this.dao = dao;
		this.converter = converter;
	}

	@Override
	public void create(User user) {
		dao.save(converter.fromUser(user));
	}

	@Override
	public void save(User user) {
		dao.save(converter.fromUser(user));
	}

	@Override
	public User get(String id) {
		Optional<UserEntity> user = dao.findById(id);
		if (user.isPresent()) {
			return converter.toUser(user.get());
		}

		throw new ObjectNotFoundException("User (id:" + id + ") does not exist.");
	}

	@Override
	public User getByEmail(String email) {
		return converter.toUser(dao.getByEmail(email));
	}
}
